# Test project :sparkles: :v:

These test project to familiarize with GitLab interface.

Here is some plan for learning process:

 * [ ] Creating project :muscle: 
 * [ ] Working  with issues :computer: :eyes:
 * [ ] Working with CI/CD :rocket:
 * [ ] Setting SSH :key:

## Docs Quick Start

#### Always start with an issue

Summarize your ideas in an issue and share it. It’s such a simple rule, but the impact is huge.

Personal access token works fine
